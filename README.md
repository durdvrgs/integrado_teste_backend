# :::Integrado::: API

## Descrição

- API para cadastro de fornecedores, registro de produtos, consulta completa ou filtrada por CNPJ, além de edição e exclusão dos fornecedores ou produtos. Construída em Node.js/MongoDB, juntamente das ferramentas Express e Mongoose.

## Instalação

- npm init
- npm install express
- npm install express-validator
- npm install nodemon
- npm install mongodb
- npm install mongoose
- npm install dotenv

## Rotas


### - Fornecedores:
__OBS__:  Para cadastro ou edição o campo __CNPJ__ aceita os seguintes formatos -"12.123.123/0001-00" ou "12123123000100 (Somente números)". E o campo __nome__ aceita qualquer String

- Method POST ::: "/api/fornecedores/" - Rota para cadastrar um novo fornecedor

```
---request body
{
    "CNPJ": "XXXXXXXXXXXXX",
    "nome": "Fornecedor 1"
}
```

```
---response 201
{
    "_id": "61015aa554d22e43fa39b95e",
    "CNPJ": "XXXXXXXXXXXXX",
    "nome": "Fornecedor 1",
    "__v": 0
}
```
- caso seja fornecido um CNPJ já cadastrado

```
---response 400
{
    "Msg": "Este Fornecedor já está cadastrados."
}
```

- Method GET ::: "/api/fornecedores/" - Retorna uma lista com todos os fornecedores cadastrados

```
---response 200
[
    {
    "_id": "61015aa554d22e43fa39b95e",
    "CNPJ": "XXXXXXXXXXXXX",
    "nome": "Fornecedor 1",
    "__v": 0
    }
    {
    "_id": "61015aa554d22e43fa39b96e",
    "CNPJ": "XXXXXXXXXXXXX",
    "nome": "Fornecedor 2",
    "__v": 0
    },
    ...
]
```

- Method GET ::: "/api/fornecedores/<int:CNPJ>" - Retorna um fornecedor pelo número de seu CNPJ

```
---response 200
{
    "_id": "61015aa554d22e43fa39b95e",
    "CNPJ": "XXXXXXXXXXXXX",
    "nome": "Fornecedor 1",
    "__v": 0
}
```

- caso seja fornecido um CNPJ não cadastrado

```
---response 404
{
    "Msg": "Fornecedor não encontrado."
}
```
- Method PUT ::: "/api/fornecedores/<str:CNPJ>/" - Rota para editar um fornecedor

```
---request body
{
    "nome": "Fornecedor 1",
}
```

```
---response 200
{
    "_id": "61015aa554d22e43fa39b95e",
    "CNPJ": "XXXXXXXXXXXXX",
    "nome": "Fornecedor 1",
    "__v": 0
}
```
- caso seja fornecido um CNPJ não cadastrado

```
---response 404
{
    "Msg": "Fornecedor não encontrado."
}
```

- Method DELETE ::: "/api/fornecedores/<str:CNPJ>/" - Rota para deletar um fornecedor

```
---response 204
{
    "No content"
}
```
- caso seja fornecido um CNPJ não cadastrado

```
---response 404
{
    "Msg": "Fornecedor não encontrado."
}
```

### - Produtos
__OBS__:  Para cadastro ou edição o campo __CNPJ__ aceita os seguintes formatos -"12.123.123/0001-00" ou "12123123000100 (Somente números)". Os Campos __codigo__ e __nome__ aceitam qualquer String

- Method POST ::: "/api/produtos/" - Rota para cadastrar um novo produto.

```
---request body
{   
    "codigo": XXXXXXXXX,
    "nome": "Produto 1"
    "CNPJ": "XXXXXXXXXXXXX",
}
```

```
---response 201
{
    "_id": "61015aa554d22e43fa39b95e",
    "codigo": XXXXXXXXX,
    "nome": "Produto 1"
    "CNPJ": "XXXXXXXXXXXXX",
    "__v": 0
}
```
- caso seja fornecido um __codigo__ ou __nome__ já cadastrado

```
---response 400
{
    "Msg": "Este Produto já está cadastrado."
}
```

- Method GET ::: "/api/produtos/" - Retorna uma lista com todos os produtos cadastrados

```
---response 200
[
    {
    "_id": "61015aa554d22e43fa39b95e",
    "codigo": XXXXXXXXX,
    "nome": "Produto 1",
    "CNPJ": "XXXXXXXXXXXXX",
    "__v": 0
    }
    {
    "_id": "61015aa554d22e43fa39b96e",
    "codigo": XXXXXXXXX,
    "nome": "Produto 2",
    "CNPJ": "XXXXXXXXXXXXX",
    "__v": 0
    },
    ...
]
```

- Method GET ::: "/api/produtos/<int:CNPJ>" - Retorna produtos pelo CNPJ

```
---response 200
[
    {
    "_id": "61015aa554d22e43fa39b95e",
    "codigo": XXXXXXXXX,
    "nome": "Produto 1",
    "CNPJ": "XXXXXXXXXXXXX",
    "__v": 0
    }
    {
    "_id": "61015aa554d22e43fa39b96e",
    "codigo": XXXXXXXXX,
    "nome": "Produto 2",
    "CNPJ": "XXXXXXXXXXXXX",
    "__v": 0
    },
    ...
]
```
- caso seja fornecido um CNPJ não cadastrado

```
---response 404
{
    "Msg": "Fornecedor não encontrado."
}
```
- Method PUT ::: "/api/produtos/<str:codigo>/" - Rota para editar um produto

```
---request body
{   
    "codigo": XXXXXXXXX,
    "nome": "Produto 1",
    "CNPJ: XXXXXXXXXXXXX
}
```

```
---response 200
{
    "_id": "61015aa554d22e43fa39b95e",
    "codigo": XXXXXXX,
    "nome": "Produto 1",
    "CNPJ: "XXXXXXXXXXXXX,
    "__v": 0
}
```
- caso seja fornecido um CODIGO não cadastrado

```
---response 404
{
    "Msg": "Produto não encontrado.
}
```
- caso seja fornecido um CNPJ não cadastrado

```
---response 404
{
    "Msg": "Fornecedor não encontrado."
}
```

- Method DELETE ::: "/api/produtos/<str:codigo>/" - Rota para deletar um produto

```
---response 204
{
    "No content"
}
```
- caso seja fornecido um CODIGO não cadastrado

```
---response 404
{
    "Msg": "Produto não encontrado.
}
```