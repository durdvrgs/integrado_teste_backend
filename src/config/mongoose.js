require('dotenv').config()

const DB_NAME = process.env.DB_NAME
const DB_CLUSTER = process.env.DB_CLUSTER 
const DB_USERNAME = process.env.DB_USERNAME
const DB_PASSWORD = process.env.DB_PASSWORD

module.exports = {
    url: `mongodb+srv://${DB_USERNAME}:${DB_PASSWORD}@${DB_CLUSTER}.m4x4j.mongodb.net/${DB_NAME}?retryWrites=true&w=majority`
}