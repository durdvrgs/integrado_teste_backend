module.exports = (mongoose) => {

    const { Schema } = mongoose

    const produtosSchema = new Schema({

        codigo: {
            type: Schema.Types.String,
            require: true,
        },

        nome: {
            type: Schema.Types.String,
            require: true,
        },

        CNPJ: {
            type: Schema.Types.String,
            require: true,
        },
    })

    mongoose.model('Produtos', produtosSchema)

    return 'Produtos'
}