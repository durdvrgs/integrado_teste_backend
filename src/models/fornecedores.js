module.exports = (mongoose) => {

    const { Schema } = mongoose

    const fornecedoresSchema = new Schema({

        CNPJ: {
            type: Schema.Types.String,
            require: true,
        },

        nome: {
            type: Schema.Types.String,
            require: true,
        }
    })

    mongoose.model('Fornecedores', fornecedoresSchema)

    return 'Fornecedores'
}