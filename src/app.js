require('dotenv').config()

const PORT = process.env.PORT
const express = require('express')
const app = express()
const bindAPIRoutes = require('./api')

app.use(express.json())
bindAPIRoutes(app)

app.listen(PORT, () => {
    console.log(`App listening on <http://localhost:${PORT}/>`)
})