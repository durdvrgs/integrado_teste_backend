const mongoose = require('mongoose');
const config = require('../config');

module.exports = async () => {

    const connection = await mongoose.connect(config.url, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })

    return connection.connection.db
}