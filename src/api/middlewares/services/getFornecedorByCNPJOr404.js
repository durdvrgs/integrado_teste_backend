const standardizeCNPJValue = require('./standardizeCNPJ')
const { Fornecedores } = require("../../../models")

module.exports = async (cnpj, res) => {
    
    const thisCNPJ = standardizeCNPJValue( cnpj )
    const fornecedor = await Fornecedores.findOne({ CNPJ: thisCNPJ })

    if (!fornecedor) {

        return res.status(404).send({
            "Msg": "Fornecedor não encontrado."
        })
    }

    return fornecedor
}