const { Produtos } = require('../../../models')

module.exports = async (codigo, res) => {
    
    const produto = await Produtos.findOne({ codigo: codigo })

    if (!produto) {

        return res.status(404).send({
            "Msg": "Produto não encontrado."
        })
    }

    return produto
}