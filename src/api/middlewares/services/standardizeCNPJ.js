module.exports = (cnpj) => {

    return cnpj.split('').filter( (chart) => !['.', '/', '-'].includes(chart) ).join('')
}