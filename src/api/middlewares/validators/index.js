const { validationResult } = require('express-validator');

module.exports.fornecedoresValidator = require('./fornecedores.validator')
module.exports.produtosValidator = require('./produtos.validator')


module.exports.validate = (req, res, next) => {

    const errors = validationResult(req)

    if (errors.isEmpty()) {
        return next()
    }

    const extractedErrors = []
    errors.array().map( (err) => extractedErrors.push({ [err.param]: err.msg }))

    return res.status(422).json({
        errors: extractedErrors
    })
}