const { body } = require('express-validator')

module.exports = (isUpdateValidator = false) => {

    const CNPJValidFormat = "^[0-9]{2}\.?[0-9]{3}\.?[0-9]{3}\/?[0-9]{4}\-?[0-9]{2}$"

    const thisValidator = [
        body('CNPJ')
        .notEmpty().withMessage('Este campo não pode ser em branco')
        .isString().withMessage('Este campo deve ser uma String')
        .matches(CNPJValidFormat).withMessage('Formato de CNPJ inválido'),

        body('nome')
        .notEmpty().withMessage('Este campo não pode ser em branco')
        .isString().withMessage('Este campo deve ser uma String')
        .isLength({max: 255}).withMessage('Máximo de 255 caracteres')
    ]

    return isUpdateValidator ? thisValidator[1] : thisValidator
}