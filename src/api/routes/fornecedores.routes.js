const { Router } = require('express')
const router = Router()


module.exports = (app) => {

    const baseUrl = '/fornecedores'
    const { list, create, retrieve, update, destroy } = require('../controllers/fornecedores.controllers')
    const { fornecedoresValidator, validate } = require('../middlewares/validators')


    router.post(baseUrl, fornecedoresValidator(), validate, create)
    router.get(baseUrl, list)
    router.get(`${baseUrl}/:CNPJ/`, retrieve)

    const isUpdateValidator = true
    router.put(`${baseUrl}/:CNPJ/`, fornecedoresValidator(isUpdateValidator), validate, update)
    router.delete(`${baseUrl}/:CNPJ/`, destroy)

    
    app.use('/api', router)
}   