const { Router } = require('express')
const router = Router()


module.exports = (app) => {

    const baseUrl = '/produtos'
    const { list, create, retrieve, update, destroy } = require("../controllers/produtos.controllers")
    const { produtosValidator ,validate } = require('../middlewares/validators')

    router.post(baseUrl, produtosValidator(), validate, create)
    router.get(baseUrl, list)
    router.get(`${baseUrl}/:CNPJ/`, retrieve)
    router.put(`${baseUrl}/:codigo/`, produtosValidator(), validate, update)
    router.delete(`${baseUrl}/:codigo/`, destroy)

    app.use('/api', router)
}