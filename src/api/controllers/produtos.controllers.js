const { Produtos } = require('../../models')

const standardizeCNPJValue = require('../middlewares/services/standardizeCNPJ')
const getFornecedorByCNPJOr404 = require('../middlewares/services/getFornecedorByCNPJOr404')
const getProductByCodeOr404 = require('../middlewares/services/getProductByCodeOr404')


module.exports.list = async (_, res) => {

    const produtos = await Produtos.find({})

    res.send(produtos)
}

module.exports.create = async (req, res) => {

    const data = req.body
    const thisCNPJ = standardizeCNPJValue(data['CNPJ'])

    await getFornecedorByCNPJOr404(thisCNPJ, res)
    
    try {
        
        const thisProdutoCode = await Produtos.findOne({ codigo: data['codigo'] })
        const thisProdutoName = await Produtos.findOne({ nome: data['nome'] })

        if (!thisProdutoCode && !thisProdutoName) {

            const newProduto = await Produtos.create({
    
                codigo: data['codigo'],
                nome: data['nome'],
                CNPJ: thisCNPJ
            })
    
            res.status(201).send(newProduto)
        }

        return res.status(400).send({
            "Msg": "Este Produto já está cadastrado"
        })


    } catch {

        return
    }
}

module.exports.retrieve = async (req, res) => {

    const thisCNPJ = req.params['CNPJ']
    const thisProdutos = await Produtos.find({ CNPJ: thisCNPJ })

    res.send(thisProdutos)
}

module.exports.update = async (req, res) => {

    const data = req.body
    const thisCode = req.params['codigo']

    await getFornecedorByCNPJOr404(data['CNPJ'], res)
    let produtoToUpdate = await getProductByCodeOr404(thisCode, res)

    try {

        Object.keys(data).forEach( (key) => {

            produtoToUpdate[key] = data[key]
        })
        await produtoToUpdate.save()

        return res.send(produtoToUpdate)

    } catch {

        return
    }
}

module.exports.destroy = async (req, res) => {

    const thisCodigo = req.params['codigo']
    let thisProduto = await getProductByCodeOr404(thisCodigo, res)

    try {

        await thisProduto.deleteOne()
        res.status(204).send()

    } catch {

        return
    }
}