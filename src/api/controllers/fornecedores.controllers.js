const { Fornecedores } = require('../../models')

const standardizeCNPJValue = require('../middlewares/services/standardizeCNPJ')
const getFornecedorByCNPJOr404 = require('../middlewares/services/getFornecedorByCNPJOr404')


module.exports.list = async (_, res) => {

    const fornecedores = await Fornecedores.find({})

    res.send(fornecedores)
}

module.exports.create = async (req, res) => {

    const data = req.body
    const thisCNPJ = standardizeCNPJValue( data['CNPJ'] )
    const fornecedorExist = await Fornecedores.findOne({ CNPJ: thisCNPJ })

    if (fornecedorExist) {

        return res.status(400).send({
            "Msg": "Este Fornecedor já está cadastrado."
        })
    }

    const newFornecedor = await Fornecedores.create({

        CNPJ: thisCNPJ,
        nome: data['nome']
    })

    res.send(newFornecedor)
}

module.exports.retrieve = async (req, res) => {

    const thisCNPJ = req.params['CNPJ']
    const thisFornecedor = await getFornecedorByCNPJOr404(thisCNPJ, res)

    try {

        res.send(thisFornecedor)
        
    } catch {

        return
    }
}

module.exports.update = async (req, res) => {

    const thisCNPJ = req.params['CNPJ']
    let thisFornecedor = await getFornecedorByCNPJOr404(thisCNPJ, res)

    try {

        thisFornecedor['nome'] = req.body['nome']
        await thisFornecedor.save()
    
        res.send(thisFornecedor)

    } catch {

        return
    }
}

module.exports.destroy = async (req, res) => {

    const thisCNPJ = req.params['CNPJ']
    let thisFornecedor = await getFornecedorByCNPJOr404(thisCNPJ, res)

    try {

        await thisFornecedor.deleteOne()
        res.status(204).send()

    } catch {

        return
    }
}