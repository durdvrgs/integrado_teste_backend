const bindFornecedoresRoutes = require('./routes/fornecedores.routes')
const bindProdutosRoutes = require('./routes/produtos.routes')

module.exports = (app) => {

    bindFornecedoresRoutes(app)
    bindProdutosRoutes(app)
}